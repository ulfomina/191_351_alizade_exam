package com.dev.exam

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.exam.adapters.NewsAdapter
import com.dev.exam.data.NewsRow
import com.dev.exam.databinding.ActivityMainBinding
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var newsList: List<NewsRow>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.topAppBar.setNavigationOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
        getNews()

    }

    fun initRV() = with(binding.recyclerView)
    {
        layoutManager = LinearLayoutManager(this@MainActivity)
        adapter = NewsAdapter(newsList)
    }

    private fun getNews(){
        val scope = CoroutineScope(Job())
        val job = scope.async {
            fetchWebsiteContents(NEWS_URL)
        }
        scope.launch(Dispatchers.Main){
            val doc = job.await()

            val newsItems = doc.select("div.card-news-list > div.card-news-list__item")
            newsList = newsItems.map{
                Log.d("ROW", NewsRow(
                    header = it.select(".card-news__text.text-default").text(),
                    date = it.select(".card-news__label.text-label").text(),
                    img = it.select(".image").attr("src")
                ).toString())
                return@map NewsRow(
                    header = it.select(".card-news__text.text-default").text(),
                    date = it.select(".card-news__label.text-label").text(),
                    img = NEWS_DOM + it.select(".image").attr("data-src")
                )
            } as MutableList<NewsRow>
            Log.d("LIST", newsList[0].toString())
            initRV()
        }

    }

    private  fun fetchWebsiteContents(url: String): Document {
        return Jsoup.connect(url).validateTLSCertificates(false).get()}


    companion object {
        const val NEWS_FILE_PATH = "data/news.txt"
        const val NEWS_DOM = "https://new.mospolytech.ru"
        const val NEWS_URL = "https://new.mospolytech.ru/news/"
    }

}