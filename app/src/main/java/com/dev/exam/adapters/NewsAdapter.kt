package com.dev.exam.adapters

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.provider.CalendarContract.CalendarCache.URI
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dev.exam.R
import com.dev.exam.data.NewsRow
import com.dev.exam.databinding.NewsItemBinding
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import java.lang.Exception

class NewsAdapter(private val news: List<NewsRow>) :
    RecyclerView.Adapter<NewsAdapter.PasswordViewHolder>() {

    class PasswordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = NewsItemBinding.bind(itemView)
        fun bind(row: NewsRow) = with(binding)
        {
            header.text = row.header
            date.text = row.date
            Log.d("IMG",row.img)
            Picasso.get()
                .load(row.img)
                .placeholder(R.drawable.ic_launcher_background)
                .into(img, object : Callback {
                    override fun onSuccess() {
                        Log.d("PICASSO", "success")
                    }

                    override fun onError(e: Exception?) {
                        e?.printStackTrace()
                    }
                })
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PasswordViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return PasswordViewHolder(view)
    }

    override fun onBindViewHolder(holder: PasswordViewHolder, position: Int) {
        holder.bind(news[position])
    }

    override fun getItemCount() = news.size

}