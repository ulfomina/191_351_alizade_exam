package com.dev.exam.data

data class NewsRow(
    val header: String,
    val date: String,
    val img: String
    )
